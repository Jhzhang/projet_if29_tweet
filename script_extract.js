db.getCollection("tweet200").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$group: {
			    // specifications
			    _id:"$user.id",
			    user_created_at:{$first:'$user.created_at'},
			    //user_id:{$first:"$user.id"},
			    tweets: {$addToSet:{id_tweet:'$id', created_tweet:"$created_at"}},
			    
			 
			    nb_pub_tweets:{$last:"$user.statuses_count"},
			    nb_follower:{$last:"$user.followers_count"},
			    nb_profil_suivi:{$last:"$user.friends_count"},
			    
			    nb_moyen_URL:{$avg:{$size:"$entities.urls"}},
			    nb_moyen_hashtage:{$avg:{$size:"$entities.hashtags"}},
			    
			    mention_avg:{$avg:{$size:"$entities.user_mentions"}},
			    nb_moyen_retweets:{$avg:"$retweet_count"},
			    nb_moyen_reponse:{$avg:"$reply_count"},
			    
			    longueur_moyen_des_tweets:{$avg:{$strLenCP:"$text"}},
			     
			        
			}
		},

		// Stage 2
		{
			$addFields: {
			   "ratio_follower_followee":{$cond: [ { $eq: [ "$nb_profil_suivi", 0 ] }, -1, { $divide:["$nb_follower", "$nb_profil_suivi"]} ]},
			   
			   "freq_friend":{$divide:["$nb_profil_suivi",{$divide:[{$subtract:[{$toDate:"2018-06-15T00:00:00Z"},{$toDate:"$user_created_at"}]},86400000]}]},
			   
			   "freq_tweet":{$divide:["$nb_pub_tweets",{$divide:[{$subtract:[{$toDate:"2018-06-15T00:00:00Z"},{$toDate:"$user_created_at"}]},86400000]}]},
			   
			}
		},

		// Stage 3
		{
			$addFields: {
			   "Agressivite":{$divide:[{$add:["$freq_friend","$freq_tweet"]},350]},
			   "Visibilite":{$divide:[{$sum:[{$multiply:["$nb_moyen_hashtage",11.4]},{$multiply:["$nb_moyen_hashtage",11.6]}]},140]}
			}
		},

		// Stage 4
		{
			$sort: {
			    //<field1>: <1|-1>,
			    //<field2>: <1|-1> ...
			    user_id:1,
			}
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
