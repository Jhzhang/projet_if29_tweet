# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy.linalg import eig


# %%
data = pd.read_csv("../csv/dataset2.csv")
data.head(2)


# %%
data2 = pd.read_csv("../csv/url_detect.csv")
data2.head(2)


# %%
group_data2 = data2.groupby(["_id","label"],as_index=False).agg({"urls":"count"})
group_data2.sort_values(by='urls', ascending=False).head(5)


# %%
good_data_url = group_data2.loc[group_data2["label"]=="good"]
good_data_url = good_data_url.rename(columns={"urls":"count_good"})
good_data_url.sort_values(by='count_good', ascending=False).head(5)


# %%
bad_data_url = group_data2.loc[group_data2["label"]=="bad"]
bad_data_url = bad_data_url.rename(columns={"urls":"count_bad"})
bad_data_url.sort_values(by='count_bad', ascending=False).head(5)


# %%
new_detect_url = pd.merge(good_data_url, bad_data_url, on='_id', how='outer')
values = {'label_x': 'good', 'label_y': 'bad', 'count_good': 0, 'count_bad': 0}
new_detect_url = new_detect_url.fillna(value=values)
new_detect_url = new_detect_url.drop_duplicates(subset='_id',keep=False,inplace=False)
new_detect_url.head(5)


# %%
new_detect_url["Danger"] = new_detect_url["count_bad"].fillna(0)/(new_detect_url["count_good"].fillna(0)+new_detect_url["count_bad"].fillna(0))
new_detect_url.head(5)


# %%
Danger_tab = new_detect_url[["_id","Danger"]]
Danger_tab.head(5)


# %%
new_data = pd.merge(data,Danger_tab,how="left",on="_id")
new_data = new_data.fillna(value={"Danger":0})
new_data.head(5)


# %%
new_data.to_csv("../csv/new_dataset.csv",index=False)


# %%
# Sélection des variables quantitatives et conversion en forme verticale
x = new_data.iloc[:,14:].values
x


# %%
# Import de la librairie StandardScaler
from sklearn.preprocessing import StandardScaler


# %%
# Application de la fonction StandardScaler (mean = 0 et standard deviation = 1)
x = StandardScaler().fit_transform(x)


# %%
# Import de la librairie ACP
from sklearn.decomposition import PCA as sklearnPCA


# %%
# Projection sur 2 axes
pca = sklearnPCA(n_components = 3)


# %%
# Prédiction des scores des deux facteurs retenus
principalComponents = pca.fit_transform(x)


# %%
# Visualisation de la matrice dans le nouveau repére
principaldf = pd.DataFrame(data = principalComponents, columns = ['Axe principal 1', 'Axe principal 2','Axe principal 3'])
principaldf.head(2)


# %%
# Constitution de la matrice finale avec la variable qualitative sp
# axis : {0/’index’, 1/’columns’}, default 0

finaldf = pd.concat([principaldf, new_data[['_id']]], axis = 1)
finaldf.head(2)


# %%
finaldf.to_csv("../csv/PCA_data.csv",index=False)


# %%
finaldf.plot.scatter(x='Axe principal 1', y='Axe principal 2')


# %%
finaldf.plot.scatter(x='Axe principal 1', y='Axe principal 3')


# %%
finaldf.plot.scatter(x='Axe principal 2', y='Axe principal 3')


# %%
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# %%
ax = plt.axes(projection='3d')
ax.scatter(x[:,0],x[:,1],x[:,2])
ax.view_init(45,35)
ax.set_xlabel('Agressivite')
ax.set_ylabel('Visibilite')
ax.set_zlabel('Danger')


# %%
#classe pour l'ACP
from sklearn.decomposition import PCA

#instanciation pour l'ACP
acp = PCA(svd_solver='full')
coord = acp.fit_transform(x)

#nombre de composantes calculées
print(acp.n_components_)
n = (acp.n_components_)


# %%
# valeurs propres
variance = acp.explained_variance_
eigval = variance*(len(x)-1)/len(x) #ajustement suite à la version 0.2
print(eigval)


# %%
#ratio de réprésentation des valeurs prores
print([ratio for ratio in acp.explained_variance_ratio_])


# %%
#scree plot
plt.plot(np.arange(1,n+1),eigval)
plt.title("Scree plot")
plt.ylabel("Eigen values")
plt.xlabel("Factor number")
plt.show()


# %%
#cumul de variance expliquée
plt.plot(np.arange(1,n+1),np.cumsum(acp.explained_variance_ratio_))
plt.title("Explained variance vs. # of factors")
plt.ylabel("Cumsum explained variance ratio")
plt.xlabel("Factor number")
plt.show()

# %% [markdown]
# __Représentation des individus__

# %%
#positionnement des individus dans le premier plan
fig, axes = plt.subplots(figsize=(8,8))
axes.set_xlim(-4,40) #même limites en abscisse
axes.set_ylim(-15,35) #et en ordonnée

# placement des points
plt.scatter(coord[:,0],coord[:,1])

#placement des étiquettes des observations
#ajouter les axes
plt.plot([-15,15],[0,0],color='silver',linestyle='-',linewidth=1)
plt.plot([0,0],[-15,15],color='silver',linestyle='-',linewidth=1)
#affichage
plt.show()


# %%
#contribution des individus dans l'inertie totale
di = np.sum(x**2,axis=1)
print(pd.DataFrame({'ID':new_data.index,'d_i':di}))


# %%
#qualité de représentation des individus - COS2
cos2 = coord**2
for j in range(3):
     cos2[:,j] = cos2[:,j]/di
print(pd.DataFrame({'id':new_data.index,'COS2_1':cos2[:,0],'COS2_2':cos2[:,1]}))

# %% [markdown]
# __Représentation des variables__

# %%
#le champ components_ de l'objet ACP (les vecteurs propres)
print(acp.components_)


# %%
#racine carrée des valeurs propres
sqrt_eigval = np.sqrt(eigval)


# %%
#corrélation des variables avec les axes
corvar = np.zeros((3,3))
for k in range(3):
     corvar[:,k] = acp.components_[k,:] * sqrt_eigval[k]

#afficher la matrice des corrélations variables x facteurs
print(corvar)


# %%
#Afin de récupérer les noms de variables quantitatives (FL,RW,CL,CW et BD)
D=new_data[new_data.columns[14:]]
D.columns


# %%
#on affiche les corrélation variables-axes pour les deux premiers axes
print(pd.DataFrame({'id':D.columns,'COR_1':corvar[:,0],'COR_2':corvar[:,1],'COR_3':corvar[:,2]}))


# %%
#cercle des corrélations pour les deux premiers axes
fig, axes = plt.subplots(figsize=(8,8))
axes.set_xlim(-1,1)
axes.set_ylim(-1,1)

# les points
plt.scatter(corvar[:,0],corvar[:,1])

#affichage des étiquettes (noms des variables)
for j in range(3):
     plt.annotate(D.columns[j],(corvar[j,0]+0.03,corvar[j,1]))

#ajouter les axes
plt.plot([-1,1],[0,0],color='silver',linestyle='-',linewidth=1)
plt.plot([0,0],[-1,1],color='silver',linestyle='-',linewidth=1)

#ajouter un cercle
cercle = plt.Circle((0,0),1,color='blue',fill=False)
axes.add_artist(cercle)

#affichage
plt.show()


# %%
#cosinus carré des variables
cos2var = corvar**2
print(pd.DataFrame({'id':D.columns,'COS2_1':cos2var[:,0],'COS2_2':cos2var[:,1],'COS2_3':cos2var[:,2]}))


# %%



