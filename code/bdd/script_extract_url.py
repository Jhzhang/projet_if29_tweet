# Requires pymongo 3.6.0+
from bson.son import SON
from pymongo import MongoClient

client = MongoClient("mongodb://host:port/")
database = client["tw_store"]
collection = database["tweet200"]

# Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

pipeline = [
    {
        u"$addFields": {
            u"total_urls": {
                u"$size": u"$entities.urls"
            }
        }
    }, 
    {
        u"$group": {
            u"_id": u"$user.id",
            u"urls": {
                u"$addToSet": u"$entities.urls.display_url"
            },
            u"sum_urls": {
                u"$sum": u"$total_urls"
            }
        }
    }, 
    {
        u"$unwind": {
            u"path": u"$urls",
            u"preserveNullAndEmptyArrays": True
        }
    }, 
    {
        u"$project": {
            u"_id": u"$_id",
            u"urls": 1.0
        }
    }, 
    {
        u"$sort": SON([ (u"sum_urls", -1), (u"_id", 1) ])
    }
]

cursor = collection.aggregate(
    pipeline, 
    allowDiskUse = True
)
try:
    for doc in cursor:
        print(doc)
finally:
    client.close()
