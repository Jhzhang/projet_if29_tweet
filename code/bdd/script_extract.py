# Requires pymongo 3.6.0+
from bson.son import SON
from pymongo import MongoClient

client = MongoClient("mongodb://host:port/")
database = client["tw_store"]
collection = database["tweet200"]

# Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

pipeline = [
    {
        u"$group": {
            u"_id": u"$user.id",
            u"user_created_at": {
                u"$first": u"$user.created_at"
            },
            u"tweets": {
                u"$addToSet": {
                    u"id_tweet": u"$id",
                    u"created_tweet": u"$created_at"
                }
            },
            u"nb_pub_tweets": {
                u"$last": u"$user.statuses_count"
            },
            u"nb_follower": {
                u"$last": u"$user.followers_count"
            },
            u"nb_profil_suivi": {
                u"$last": u"$user.friends_count"
            },
            u"nb_moyen_URL": {
                u"$avg": {
                    u"$size": u"$entities.urls"
                }
            },
            u"nb_moyen_hashtage": {
                u"$avg": {
                    u"$size": u"$entities.hashtags"
                }
            },
            u"mention_avg": {
                u"$avg": {
                    u"$size": u"$entities.user_mentions"
                }
            },
            u"nb_moyen_retweets": {
                u"$avg": u"$retweet_count"
            },
            u"nb_moyen_reponse": {
                u"$avg": u"$reply_count"
            },
            u"longueur_moyen_des_tweets": {
                u"$avg": {
                    u"$strLenCP": u"$text"
                }
            }
        }
    }, 
    {
        u"$addFields": {
            u"ratio_follower_followee": {
                u"$cond": [
                    {
                        u"$eq": [
                            u"$nb_profil_suivi",
                            0.0
                        ]
                    },
                    -1.0,
                    {
                        u"$divide": [
                            u"$nb_follower",
                            u"$nb_profil_suivi"
                        ]
                    }
                ]
            },
            u"freq_friend": {
                u"$divide": [
                    u"$nb_profil_suivi",
                    {
                        u"$divide": [
                            {
                                u"$subtract": [
                                    {
                                        u"$toDate": u"2018-06-15T00:00:00Z"
                                    },
                                    {
                                        u"$toDate": u"$user_created_at"
                                    }
                                ]
                            },
                            8.64E7
                        ]
                    }
                ]
            },
            u"freq_tweet": {
                u"$divide": [
                    u"$nb_pub_tweets",
                    {
                        u"$divide": [
                            {
                                u"$subtract": [
                                    {
                                        u"$toDate": u"2018-06-15T00:00:00Z"
                                    },
                                    {
                                        u"$toDate": u"$user_created_at"
                                    }
                                ]
                            },
                            8.64E7
                        ]
                    }
                ]
            }
        }
    }, 
    {
        u"$addFields": {
            u"Agressivite": {
                u"$divide": [
                    {
                        u"$add": [
                            u"$freq_friend",
                            u"$freq_tweet"
                        ]
                    },
                    350.0
                ]
            },
            u"Visibilite": {
                u"$divide": [
                    {
                        u"$sum": [
                            {
                                u"$multiply": [
                                    u"$nb_moyen_hashtage",
                                    11.4
                                ]
                            },
                            {
                                u"$multiply": [
                                    u"$nb_moyen_hashtage",
                                    11.6
                                ]
                            }
                        ]
                    },
                    140.0
                ]
            }
        }
    }, 
    {
        u"$sort": SON([ (u"user_id", 1) ])
    }
]

cursor = collection.aggregate(
    pipeline, 
    allowDiskUse = False
)
try:
    for doc in cursor:
        print(doc)
finally:
    client.close()
