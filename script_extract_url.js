db.getCollection("tweet200").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$addFields: {
			    total_urls:{$size:"$entities.urls"}
			}
		},

		// Stage 2
		{
			$group: {
			    _id: "$user.id",
			    //<field1>: { <accumulator1> : <expression1> },
			    //...
			    urls:{$addToSet:"$entities.urls.display_url"},
			    sum_urls:{$sum:"$total_urls"},
			}
		},

		// Stage 3
		{
			$unwind: {
			    path: "$urls",
			    preserveNullAndEmptyArrays: true // optional
			}
		},

		// Stage 4
		{
			$project: {
			    // specifications
			    _id:"$_id",
			    "urls":1
			}
		},

		// Stage 5
		{
			$sort: {
			    //<field1>: <1|-1>,
			    //<field2>: <1|-1> ...
			    sum_urls:-1,
			    _id:1
			
			}
		},
	],

	// Options
	{
		allowDiskUse: true
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
